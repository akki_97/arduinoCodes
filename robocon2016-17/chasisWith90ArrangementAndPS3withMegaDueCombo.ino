#include<Wire.h>
int frontMotorDirection = 24;
int fronMotorPWM = 4;

int leftMotorDirection = 23;
int leftMotorPWM = 3;

int backMotorDirection = 25;
int backMotorPWM = 6;

int rightMotorDirection = 22;
int rightMotorPWM = 7;

int speed = 250;
int speedForTopRight = speed;
void setup()
{
  pinMode(frontMotorDirection, OUTPUT);
  pinMode(fronMotorPWM, OUTPUT);

  pinMode(leftMotorDirection, OUTPUT);
  pinMode(leftMotorPWM, OUTPUT);

  pinMode(backMotorDirection, OUTPUT);
  pinMode(backMotorPWM, OUTPUT);

  pinMode(rightMotorDirection, OUTPUT);
  pinMode(rightMotorPWM, OUTPUT);


  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(115200);

}


void loop()
{
  delay(100); // Wait for checking of transmission connection
}

void receiveEvent(int howMany)
{
    while(1 < Wire.available() ) // Necessary for reading accurate values from Due
    {

      char c  = Wire.read();
      Serial.println(c);
    }

    int x = Wire.read();
    Serial.print(x);



    if (x == 10)
    {
      upMovement();
      Serial.println("Moving Forward");
    }
    else if(x == 20)
    {
      leftMovement();
      Serial.println("Moving Left");
    }
    else if(x == 30)
    {
      downMovement();
      Serial.println("Moving Reverse");
    }
    else if(x == 40)
    {
      rightMovement();
      Serial.println("Moving Right");
    }
    else if(x == 50)
    {
      allStop();
      Serial.println("Stopping Everything");
    }
    else if (x == 60)
    {
      wholeBotantiClockwise();
      Serial.println("antiClockwise Motion");
    }
    else if( x == 70)
    {
      wholeBotClockwise();
      Serial.println("Clockwise Motion");
    }
}


void upMovement()
{

    digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
    analogWrite(fronMotorPWM, 0);

    digitalWrite(leftMotorDirection, HIGH);
    analogWrite(leftMotorPWM, speed);

    digitalWrite(backMotorDirection, LOW);
    analogWrite(backMotorPWM, 0);

    digitalWrite(rightMotorDirection, LOW);
    analogWrite(rightMotorPWM, speed);
}


void leftMovement()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

}



void downMovement()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, 0);

  digitalWrite(leftMotorDirection, LOW);
  analogWrite(leftMotorPWM, speed);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, 0);

  digitalWrite(rightMotorDirection, HIGH);
  analogWrite(rightMotorPWM, speed);



}


void rightMovement()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);

}



void allStop()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, 0);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, 0);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, 0);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, 0);



}


void wholeBotClockwise()
{
  digitalWrite(frontMotorDirection,HIGH); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, HIGH);
  analogWrite(leftMotorPWM, speed);

  digitalWrite(backMotorDirection, HIGH);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, HIGH);
  analogWrite(rightMotorPWM, speed);


}

void wholeBotantiClockwise()
{
  digitalWrite(frontMotorDirection,LOW); // Left Top Motor clockwise
  analogWrite(fronMotorPWM, speed);

  digitalWrite(leftMotorDirection, LOW);
  analogWrite(leftMotorPWM, speed);

  digitalWrite(backMotorDirection, LOW);
  analogWrite(backMotorPWM, speed);

  digitalWrite(rightMotorDirection, LOW);
  analogWrite(rightMotorPWM, speed);

}
