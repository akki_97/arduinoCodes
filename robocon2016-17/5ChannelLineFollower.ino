// 5 Channel Line FOllower Robot

// 5 Channel Line Sensors
int l2 = 2;
int l1 = 3;
int c = 4;
int r1 = 5;
int r2 = 6;

// Left and right sensor.
int leftMotorForward = 8;
int leftMotorReverse = 9;

int rightMotorForward = 38;
int rightMotorReverse = 39;

void setup()
{
  pinMode(l2,INPUT);
  pinMode(l1,INPUT);
  pinMode(c,INPUT);
  pinMode(r1,INPUT);
  pinMode(r2,INPUT);

  pinMode(leftMotorForward, OUTPUT);
  pinMode(leftMotorReverse, OUTPUT);
  pinMode(rightMotorForward, OUTPUT);
  pinMode(rightMotorReverse, OUTPUT);

  Serial.begin(115200);
}

void loop()
{

Serial.print(digitalRead(l2));
Serial.print(' ');
Serial.print(digitalRead(l1));
Serial.print(' ');
Serial.print(digitalRead(c));
Serial.print(' ');
Serial.print(digitalRead(r1));
Serial.print(' ');
Serial.print(digitalRead(r2));
Serial.print(' ');
Serial.print("\n");

int L2 = digitalRead(l2);
int L1 = digitalRead(l1);
int C = digitalRead(c);
int R1 = digitalRead(r1);
int R2 = digitalRead(r2);



if (L2 == 0 && L1 == 0 && C == 0 && R1 == 0 && R2 == 0) // NO Line 1
{
  digitalWrite(leftMotorForward,LOW);
  digitalWrite(leftMotorReverse,HIGH);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,HIGH);
}
else if (L2 == 0 && L1 == 0 && C == 0 && R1 == 0 && R2 == 1) // RIGHT SIDE Line 2
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 0 && C == 0 && R1 == 1 && R2 == 0) // RIGHT SIDE Line 3
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 0 && C == 0 && R1 == 1 && R2 == 1) // RIGHT SIDE Line 4
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 0 && C == 1 && R1 == 0 && R2 == 0) // CENTER LINE forward Line 5
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 0 && C == 1 && R1 == 1 && R2 == 0) // RIGHT SIDE Line 6
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 0 && C == 1 && R1 == 1 && R2 == 1) // RIGHT SIDE Line 7
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 1 && C == 0 && R1 == 0 && R2 == 0) // left SIDE Line 8
{
  digitalWrite(leftMotorForward,LOW);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 1 && C == 1 && R1 == 0 && R2 == 0) // LEFT SIDE Line 9
{
  digitalWrite(leftMotorForward,LOW);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 1 && C == 1 && R1 == 1 && R2 == 0) // BROAD CENTER LINE Line 10
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 0 && L1 == 1 && C == 1 && R1 == 1 && R2 == 1) // RIGHT SIDE Line with slight left Line 11
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 1 && L1 == 0 && C == 0 && R1 == 0 && R2 == 0) // sharp left SIDE Line 12
{
  digitalWrite(leftMotorForward,LOW);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 1 && L1 == 1 && C == 0 && R1 == 0 && R2 == 0) // left SIDE Line 13
{
  digitalWrite(leftMotorForward,LOW);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 1 && L1 == 1 && C == 1 && R1 == 0 && R2 == 0) // sharp left Line 14
{
  digitalWrite(leftMotorForward,LOW);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 1 && L1 == 1 && C == 1 && R1 == 1 && R2 == 0) // sharp left side with slight right line 15
{
  digitalWrite(leftMotorForward,LOW);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,HIGH);
  digitalWrite(rightMotorReverse,LOW);
}
else if (L2 == 1 && L1 == 1 && C == 1 && R1 == 1 && R2 == 1) // CROSS SECTION Line 16
{
  digitalWrite(leftMotorForward,HIGH);
  digitalWrite(leftMotorReverse,LOW);
  digitalWrite(rightMotorForward,LOW);
  digitalWrite(rightMotorReverse,LOW);
}


}
