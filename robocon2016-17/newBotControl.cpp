// New Motor chasis control program

int chasisWheelsPin1 = 5;
int chasisWheelsPin2 = 6;

int centerWheelMotorPin1 = 2;
int centerWheelMotorPin2 = 3;

void setup()
{
  pinMode(chasisWheelsPin1, OUTPUT);
  pinMode(chasisWheelsPin2,OUTPUT);

  pinMode(centerWheelMotorPin1,OUTPUT);
  pinMode(centerWheelMotorPin2,OUTPUT);

  pinMode(13, OUTPUT);

}
void loop()
{
  analogWriteControl(100);
}



void digitalWriteControl()
{
  digitalWrite(chasisWheelsPin1,HIGH); // All chasis motor Forward
  digitalWrite(chasisWheelsPin2,LOW);
  delay(10000);

  digitalWrite(chasisWheelsPin1,LOW); // All chasis motor Stop
  digitalWrite(chasisWheelsPin2,LOW);
  delay(4000);

  digitalWrite(centerWheelMotorPin1,HIGH); // Center wheel Forward
  digitalWrite(centerWheelMotorPin2,LOW);
  delay(4000);

  digitalWrite(centerWheelMotorPin1, LOW);//center motor wheel STOP
  digitalWrite(centerWheelMotorPin2, LOW);
  delay(4000);

  digitalWrite(chasisWheelsPin1,LOW); // All chasis motor wheel Reverse
  digitalWrite(chasisWheelsPin2,HIGH);
  delay(10000);

  digitalWrite(chasisWheelsPin1,LOW); // All chasis motor off
  digitalWrite(chasisWheelsPin2,LOW);
  delay(4000);

  digitalWrite(centerWheelMotorPin1,LOW); // Center motor Reverse
  digitalWrite(centerWheelMotorPin2,HIGH);
  delay(4000);

  digitalWrite(centerWheelMotorPin1, LOW);//center motor wheel STOP
  digitalWrite(centerWheelMotorPin2, LOW);
  delay(4000);
}

void analogWriteControl(int speed)
{
  analogWrite(chasisWheelsPin1,speed); // ALL Forward
  analogWrite(chasisWheelsPin2,0);
  delay(4000);

  analogWrite(chasisWheelsPin1,0); // All Stop
  analogWrite(chasisWheelsPin2,0);
  delay(4000);

  analogWrite(chasisWheelsPin1,0); // All Reverse
  analogWrite(chasisWheelsPin2,speed);
  delay(4000);

  analogWrite(chasisWheelsPin1,0); // All Stop
  analogWrite(chasisWheelsPin2,0);
  delay(4000);

}

void switchOFFEverything()
{
  // no conditions
  digitalWrite(13,HIGH);
  delay(500);
  digitalWrite(13,LOW);
  delay(500);

}
